$(document).ready(function() {
  
  window.onscroll = function() {
    // scrollFunction();
  }

  AOS.init();
});

function scrollFunction() {
  if(document.body.scrollTop > 10 || document.documentElement.scrollTop > 1) {
    $(".navbar").css("background-color", "rgba(0, 0, 0, 0.7)");
  } else {
    $(".navbar").css("background-color", "rgba(0, 0, 0, 0)");
  }
}