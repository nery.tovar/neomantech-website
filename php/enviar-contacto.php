<?php
  ini_set( 'display_errors', 1 );
  error_reporting( E_ALL );
  $from = "web@neomantech.com";
  $to = "contacto@neomantech.com";
  $subject = 'Mensaje enviado desde el sitio web';
  $message = 'Nombre completo: ' . $_POST['nombre'] . PHP_EOL . 
            'Telefono: ' . $_POST['telefono'] . PHP_EOL . 
            'e-mail: ' . $_POST['correo'] . PHP_EOL .
            'Asunto: ' . $_POST['asunto'] . PHP_EOL .
            'Mensaje: ' . $_POST['mensaje'];
  $headers = "From:" . $from;
  mail($to,$subject,$message, $headers);
  header("Location: https://neomantech.com/contacto.html");
  die();
?>